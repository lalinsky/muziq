
Required packages:

    #!sh
    sudo apt-get install python-sqlalchemy python-flask python-redis \
                         python-pysolr python-wtforms python-lxml \
                         python-psycopg2

Database:

    #!sql
	CREATE SCHEMA muziq;
	GRANT ALL ON SCHEMA muziq TO muziq;
    GRANT USAGE ON SCHEMA musicbrainz TO muziq;
	GRANT SELECT ON ALL TABLES IN SCHEMA musicbrainz TO muziq;

