#!/usr/bin/env python

from muziq.web import app

app.run(host='0.0.0.0', debug=True, use_reloader=True, processes=10)

