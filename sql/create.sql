BEGIN;

SET search_path=muziq;


DROP TABLE IF EXISTS medialib_release;
DROP TABLE IF EXISTS medialib_file;
DROP TABLE IF EXISTS medialib_directory;


CREATE TABLE medialib_directory (
    id SERIAL PRIMARY KEY,
    parent INTEGER NULL REFERENCES medialib_directory (id),
    name TEXT NOT NULL,
    CHECK ((parent IS NULL AND name = '') OR (parent IS NOT NULL AND name != '')),
    UNIQUE (parent, name)
);

CREATE TABLE medialib_file (
    id SERIAL PRIMARY KEY,
    parent INTEGER NOT NULL REFERENCES medialib_directory (id),
    name TEXT NOT NULL CHECK (name != ''),
    added TIMESTAMP WITH TIME ZONE NOT NULL,
    updated TIMESTAMP WITH TIME ZONE NOT NULL,
    mb_recording UUID NOT NULL,
    mb_release UUID NOT NULL,
    mb_track_no INTEGER NOT NULL,
    mb_medium_no INTEGER NOT NULL,
    UNIQUE (parent, name)
);

CREATE TABLE medialib_release (
    mb_release UUID PRIMARY KEY,
    added TIMESTAMP WITH TIME ZONE NOT NULL,
    updated TIMESTAMP WITH TIME ZONE NOT NULL
);


CREATE INDEX medialib_file_idx_mb_release ON medialib_file (mb_release);
CREATE INDEX medialib_file_idx_mb_recording ON medialib_file (mb_recording);
CREATE INDEX medialib_file_idx_added ON medialib_file (added);

CREATE INDEX medialib_release_idx_mb_release ON medialib_release (mb_release);
CREATE INDEX medialib_release_idx_added ON medialib_release (added);


CREATE OR REPLACE FUNCTION medialib_file_tr_b_ins() RETURNS trigger AS $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM muziq.medialib_release WHERE mb_release = NEW.mb_release) THEN
        INSERT INTO muziq.medialib_release (mb_release, added, updated) VALUES (NEW.mb_release, NEW.added, NEW.updated);
    ELSE
        UPDATE muziq.medialib_release SET updated = NEW.updated WHERE mb_release = NEW.mb_release;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER medialib_file_tr_b_ins BEFORE INSERT ON medialib_file FOR EACH ROW EXECUTE PROCEDURE medialib_file_tr_b_ins();


CREATE OR REPLACE FUNCTION medialib_file_tr_b_upd() RETURNS trigger AS $$
BEGIN
    IF NEW.mb_release != OLD.mb_release THEN
        IF NOT EXISTS (SELECT 1 FROM muziq.medialib_file WHERE mb_release = OLD.mb_release AND id != OLD.id) THEN
            DELETE FROM muziq.medialib_release WHERE mb_release = OLD.mb_release;
        END IF;
        IF NOT EXISTS (SELECT 1 FROM medialib_release WHERE mb_release = NEW.mb_release) THEN
            INSERT INTO muziq.medialib_release (mb_release, added, updated) VALUES (NEW.mb_release, NEW.added, NEW.updated);
        END IF;
    ELSE
        UPDATE muziq.medialib_release SET updated = NEW.updated WHERE mb_release = NEW.mb_release;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER medialib_file_tr_b_upd BEFORE UPDATE ON medialib_file FOR EACH ROW EXECUTE PROCEDURE medialib_file_tr_b_upd();


CREATE OR REPLACE FUNCTION medialib_file_tr_b_del() RETURNS trigger AS $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM muziq.medialib_file WHERE mb_release = OLD.mb_release AND id != OLD.id) THEN
        DELETE FROM muziq.medialib_release WHERE mb_release = OLD.mb_release;
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER medialib_file_tr_b_del BEFORE DELETE ON medialib_file FOR EACH ROW EXECUTE PROCEDURE medialib_file_tr_b_del();


COMMIT;

