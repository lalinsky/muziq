#!/usr/bin/env python

import redis
from muziq import settings

r = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT)
queue = r.pubsub()
queue.subscribe('np')
for message in queue.listen():
    user = message['data']
    print message
    print r.get('user:%s:np' % user)

