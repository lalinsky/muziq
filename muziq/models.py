from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.sql import text


Base = declarative_base()


class MedialibDirectory(Base):
    __tablename__ = 'medialib_directory'
    __table_args__ = {'schema': 'muziq'}

    id = Column(Integer, primary_key=True)
    parent_id = Column('parent', Integer, ForeignKey('muziq.medialib_directory.id'))
    parent = relationship("MedialibDirectory", backref="directories", remote_side=[id])
    name = Column(String, nullable=False)


class MedialibFile(Base):
    __tablename__ = 'medialib_file'
    __table_args__ = {'schema': 'muziq'}

    id = Column(Integer, primary_key=True)
    parent_id = Column('parent', Integer, ForeignKey('muziq.medialib_directory.id'))
    parent = relationship("MedialibDirectory", backref="files")
    name = Column(String, nullable=False)

    added = Column(DateTime, nullable=False)
    updated = Column(DateTime, nullable=False)

    mb_recording = Column(String, nullable=False)
    mb_release = Column(String, nullable=False)
    mb_track_no = Column(Integer, nullable=False)
    mb_medium_no = Column(Integer, nullable=False)


class MedialibRelease(Base):
    __tablename__ = 'medialib_release'
    __table_args__ = {'schema': 'muziq'}

    mb_release_id = Column('mb_release', UUID, ForeignKey('musicbrainz.release.gid'), primary_key=True)
    mb_release = relationship('Release', innerjoin=True)
    added = Column(DateTime, nullable=False)


class MedialibRecording(Base):
    __tablename__ = 'medialib_recording'
    __table_args__ = {'schema': 'muziq'}

    mb_release = Column(String, primary_key=True)
    added = Column(DateTime, nullable=False)


class Release(Base):
    __tablename__ = 'release'
    __table_args__ = {'schema': 'musicbrainz'}

    id = Column(Integer, primary_key=True)
    gid = Column(UUID, nullable=False, unique=True)
    name_id = Column('name', String, ForeignKey('musicbrainz.release_name.id'), nullable=False)
    name = relationship('ReleaseName', lazy='joined', innerjoin=True)
    artist_credit_id = Column('artist_credit', String, ForeignKey('musicbrainz.artist_credit.id'), nullable=False)
    artist_credit = relationship('ArtistCredit', innerjoin=True)

    mediums = relationship('Medium', order_by='Medium.position', backref='release')


class ReleaseName(Base):
    __tablename__ = 'release_name'
    __table_args__ = {'schema': 'musicbrainz'}

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)


class ArtistName(Base):
    __tablename__ = 'artist_name'
    __table_args__ = {'schema': 'musicbrainz'}

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)


class ArtistCredit(Base):
    __tablename__ = 'artist_credit'
    __table_args__ = {'schema': 'musicbrainz'}

    id = Column(Integer, primary_key=True)
    name_id = Column('name', String, ForeignKey('musicbrainz.artist_name.id'), nullable=False)
    name = relationship('ArtistName', innerjoin=True)

    artists = relationship('ArtistCreditName', order_by='ArtistCreditName.position', backref='artist_credit')


class ArtistCreditName(Base):
    __tablename__ = 'artist_credit_name'
    __table_args__ = {'schema': 'musicbrainz'}

    artist_credit_id = Column('artist_credit', Integer, ForeignKey('musicbrainz.artist_credit.id'), nullable=False, primary_key=True)
    position = Column(Integer, primary_key=True)
    artist_id = Column('artist', String, ForeignKey('musicbrainz.artist.id'), nullable=False)
    artist = relationship('Artist', lazy='joined', innerjoin=True)
    name_id = Column('name', String, ForeignKey('musicbrainz.artist_name.id'), nullable=False)
    name = relationship('ArtistName', lazy='joined', innerjoin=True)
    join_phrase = Column(String)


class Artist(Base):
    __tablename__ = 'artist'
    __table_args__ = {'schema': 'musicbrainz'}

    id = Column(Integer, primary_key=True)
    gid = Column(UUID, nullable=False, unique=True)
    name_id = Column('name', String, ForeignKey('musicbrainz.artist_name.id'), nullable=False)
    name = relationship('ArtistName', lazy='joined', innerjoin=True)


class Medium(Base):
    __tablename__ = 'medium'
    __table_args__ = {'schema': 'musicbrainz'}

    id = Column(Integer, primary_key=True)
    release_id = Column('release', Integer, ForeignKey('musicbrainz.release.id'), nullable=False)
    position = Column(Integer, nullable=False)
    name = Column(String)

    tracks = relationship('Track', order_by='Track.position', backref='medium')


class TrackName(Base):
    __tablename__ = 'track_name'
    __table_args__ = {'schema': 'musicbrainz'}

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)


class Track(Base):
    __tablename__ = 'track'
    __table_args__ = {'schema': 'musicbrainz'}

    id = Column(Integer, primary_key=True)
    gid = Column(UUID, nullable=False, unique=True)
    medium_id = Column('medium', String, ForeignKey('musicbrainz.medium.id'), nullable=False)
    position = Column(Integer, nullable=False)
    name_id = Column('name', String, ForeignKey('musicbrainz.track_name.id'), nullable=False)
    name = relationship('TrackName', lazy='joined', innerjoin=True)
    artist_credit_id = Column('artist_credit', String, ForeignKey('musicbrainz.artist_credit.id'), nullable=False)
    artist_credit = relationship('ArtistCredit', innerjoin=True)
    recording_id = Column('recording', String, ForeignKey('musicbrainz.recording.id'), nullable=False)
    recording = relationship('Recording', innerjoin=True)
    length = Column(Integer)


class Recording(Base):
    __tablename__ = 'recording'
    __table_args__ = {'schema': 'musicbrainz'}

    id = Column(Integer, primary_key=True)
    gid = Column(UUID, nullable=False, unique=True)
    name_id = Column('name', String, ForeignKey('musicbrainz.track_name.id'), nullable=False)
    name = relationship('TrackName', lazy='joined', innerjoin=True)
    artist_credit_id = Column('artist_credit', String, ForeignKey('musicbrainz.artist_credit.id'), nullable=False)
    artist_credit = relationship('ArtistCredit', innerjoin=True)
    length = Column(Integer)


class CoverArt(Base):
    __tablename__ = 'cover_art'
    __table_args__ = {'schema': 'musicbrainz'}

    id = Column(Integer, primary_key=True)
    release_id = Column('release', String, ForeignKey('musicbrainz.release.id'), nullable=False)
    release = relationship('Release', innerjoin=True)
    mime_type = Column(String)

