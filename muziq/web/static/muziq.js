(function () {

    function callVLC(request, params, callback) {
        var port = 8091,
            url = 'http://127.0.0.1:' + port + '/requests/' + request + '.xml?' + $.param(params);
        $.ajax({ url: url, dataType: 'xml' })
            .done(callback)
            .fail(function () { console.log('VLC call failed') });
    }

    $(document).ready(function () {
        $('.vlc-play-xspf').click(function () {
            var url = this.href;
            callVLC('status', { command: 'pl_empty' }, function (data) {
                callVLC('status', { command: 'in_play', input: url }, function (data) {
                });
            });
            return false;
        });
        $('.vlc-pause').click(function () {
            callVLC('status', { command: 'pl_pause' });
            return false;
        });
        $('.vlc-next').click(function () {
            callVLC('status', { command: 'pl_next' });
            return false;
        });
        $('.vlc-previous').click(function () {
            callVLC('status', { command: 'pl_previous' });
            return false;
        });
    });

})();
