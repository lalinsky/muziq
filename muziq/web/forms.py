from wtforms import Form, TextField
from wtforms.validators import Required


class SearchForm(Form):
    q = TextField(validators=[Required()])

