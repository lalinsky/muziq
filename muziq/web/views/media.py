import os
import urllib
import mimetypes
from lxml import etree
from sqlalchemy.orm import joinedload, subqueryload, subqueryload_all
from flask import Blueprint, g, request, abort, render_template, make_response, url_for
from muziq import db
from muziq.models import MedialibFile, MedialibDirectory

media_pages = Blueprint('media', __name__)


@media_pages.route('/file/<int:id>')
def file(id):
    file = db.session.query(MedialibFile).get(id)
    print id, file
    if file is None:
        return abort(404)

    url = file.name
    parent_id = file.parent_id
    while parent_id:
        directory = db.session.query(MedialibDirectory).get(parent_id)
        url = '%s/%s' % (directory.name, url)
        parent_id = directory.parent_id

    content_type = mimetypes.guess_type(url)[0] or 'audio/mpeg'

    response = make_response()
    response.headers['Content-Type'] = content_type
    response.headers['Content-Disposition'] = 'attachment; filename="%s"' % file.name
    response.headers['Cache-Control'] = 'private'
    response.headers['X-Accel-Redirect'] = '/internal/stream%s' % url
    return response

