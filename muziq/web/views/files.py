from sqlalchemy import sql, distinct
from sqlalchemy.orm import joinedload, subqueryload, subqueryload_all
from flask import Blueprint, g, request, abort, render_template, make_response
from muziq import db
from muziq.models import MedialibFile, MedialibDirectory

files_pages = Blueprint('files', __name__)


@files_pages.route('/')
@files_pages.route('/<int:directory_id>')
def index(directory_id=None):
    if directory_id is None:
        directory = db.session.query(MedialibDirectory).filter_by(parent=None, name='').one()
    else:
        directory = db.session.query(MedialibDirectory).get(directory_id)

    directories = db.session.query(MedialibDirectory).filter_by(parent=directory).order_by(MedialibDirectory.name)
    files = db.session.query(MedialibFile).filter_by(parent=directory).order_by(MedialibFile.name)

    return render_template('files.html', directory=directory, directories=directories, files=files)

