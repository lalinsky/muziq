import os
import json
import urllib2
from sqlalchemy import sql, distinct
from sqlalchemy.orm import joinedload, subqueryload, subqueryload_all
from lxml import etree
from flask import Blueprint, g, request, abort, render_template, make_response, current_app, url_for
from muziq import tables as t
from muziq import db
from muziq.models import Artist, ArtistCredit, ArtistCreditName, Release, MedialibRelease
from muziq.web.utils import cache_image

artist_pages = Blueprint('artist', __name__)


@artist_pages.route('/<string:id>')
def index(id):
    artist = db.session.query(Artist).filter_by(gid=id).first()
    if artist is None:
        return abort(404)

    artist_credits = db.session.query(ArtistCreditName.artist_credit_id).\
        filter(ArtistCreditName.artist_id == artist.id).\
        subquery()

    medialib_releases = db.session.query(MedialibRelease.mb_release_id).\
        subquery()

    releases = db.session.query(Release).\
        filter(Release.artist_credit_id.in_(artist_credits)).\
        filter(Release.gid.in_(medialib_releases)).\
        options(joinedload('artist_credit')).\
        options(subqueryload('artist_credit.artists')).\
        all()

    return render_template('artist.html', artist=artist, releases=releases)


@artist_pages.route('/<string:id>/image')
@artist_pages.route('/<string:id>/image/<int:size>')
def image(id, size=500):
    cache_dir = os.path.join(current_app.config['CACHE_DIR'], 'artist_image')
    fallback_url = url_for('static', filename='no-cover.png')

    def fetch_artist_image():
        url = 'http://api.fanart.tv/webservice/artist/%s/%s/JSON/artistthumb/1/1/' % (current_app.config['FANART_API_KEY'], id)
        try:
            artists = json.load(urllib2.urlopen(url))
        except urllib2.URLError:
            return None

        if not artists:
            return None

        images = artists.values()[0]['artistthumb']
        if not images:
            return None

        try:
            return urllib2.urlopen(images[0]['url'])
        except urllib2.URLError:
            return None

    return cache_image(cache_dir, fetch_artist_image, fallback_url, id, size)


def foo():
    source = t.artist.\
        join(t.artist_name, t.artist.c.name_id == t.artist_name.c.id)
    query = sql.select([
        t.artist.c.gid.label('id'),
        t.artist_name.c.name,
    ], from_obj=source)
    query = query.where(t.artist.c.gid == id)
    artist = g.db.execute(query).fetchone()
    if artist is None:
        return abort(404)

    source = t.release_group.\
        join(t.release_name, t.release_group.c.name_id == t.release_name.c.id).\
        join(t.release_group_meta, t.release_group.c.id == t.release_group_meta.c.id).\
        join(t.release_group_primary_type, t.release_group.c.type_id == t.release_group_primary_type.c.id).\
        join(t.artist_credit_name, t.release_group.c.artist_credit_id == t.artist_credit_name.c.artist_credit_id).\
        join(t.artist, t.artist_credit_name.c.artist_credit_id == t.artist.c.id)
    query = sql.select([
        t.release_group.c.gid.label('id'),
        t.release_name.c.name,
        t.release_group_primary_type.c.name.label('type'),
        t.release_group_meta.c.first_release_date_year,
        t.release_group_meta.c.first_release_date_month,
        t.release_group_meta.c.first_release_date_day,
    ], from_obj=source)
    query = query.where(t.artist.c.gid == id)
    query = query.where(t.release_group_meta.c.first_release_date_year != None)
    query = query.order_by(
        t.release_group_meta.c.first_release_date_year.desc(),
        t.release_group_meta.c.first_release_date_month.desc(),
        t.release_group_meta.c.first_release_date_day.desc())
    limit = min(100, request.args.get('limit', 100, type=int))
    query = query.limit(limit)
    albums = g.db.execute(query).fetchall()

    return render_template('artist.html', artist=artist, albums=albums)


@artist_pages.route('/<string:id>/new-releases.rss')
def new_releases_rss(id):
    source = t.release_group.\
        join(t.release_name, t.release_group.c.name_id == t.release_name.c.id).\
        join(t.release_group_meta, t.release_group.c.id == t.release_group_meta.c.id).\
        join(t.release_group_primary_type, t.release_group.c.type_id == t.release_group_primary_type.c.id).\
        join(t.artist_credit_name, t.release_group.c.artist_credit_id == t.artist_credit_name.c.artist_credit_id).\
        join(t.artist, t.artist_credit_name.c.artist_credit_id == t.artist.c.id)
    query = sql.select([
        t.release_group.c.gid,
        t.release_name.c.name,
        t.release_group_primary_type.c.name.label('type'),
        t.release_group_meta.c.first_release_date_year,
        t.release_group_meta.c.first_release_date_month,
        t.release_group_meta.c.first_release_date_day,
    ], from_obj=source)
    query = query.where(t.artist.c.gid == id)
    query = query.where(t.release_group_meta.c.first_release_date_year != None)
    query = query.order_by(
        t.release_group_meta.c.first_release_date_year.desc(),
        t.release_group_meta.c.first_release_date_month.desc(),
        t.release_group_meta.c.first_release_date_day.desc())
    limit = min(100, request.args.get('limit', 10, type=int))
    query = query.limit(limit)
    feed = etree.Element('feed', nsmap={None: 'http://www.w3.org/2005/Atom'})
    etree.SubElement(feed, 'title').text = 'New Releases'
    for row in g.db.execute(query):
        entry = etree.SubElement(feed, 'entry')
        etree.SubElement(entry, 'title').text = '%s %s [%s] [%s]' % ('', row['name'], row['first_release_date_year'], row['type'])
        url = 'http://muziq.eu/album/' + row['gid']
        etree.SubElement(entry, 'link', href=url)
    return etree.tostring(feed, pretty_print=True)

