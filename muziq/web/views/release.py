import os
import urllib2
import shutil
import subprocess
from lxml import etree
from sqlalchemy.orm import joinedload, subqueryload, subqueryload_all, joinedload_all
from flask import Blueprint, g, request, abort, render_template, make_response, send_file, url_for, redirect, current_app
from muziq import db
from muziq.models import Release, MedialibFile, CoverArt
from muziq.web.utils import cache_image

release_pages = Blueprint('release', __name__)


@release_pages.route('/<string:id>')
def index(id):
    release = db.session.query(Release).filter_by(gid=id).\
        options(joinedload('artist_credit')).\
        options(subqueryload('artist_credit.artists')).\
        options(joinedload('mediums.tracks.artist_credit')).\
        options(subqueryload_all('mediums.tracks.artist_credit.artists')).\
        first()
    if release is None:
        return abort(404)

    files = db.session.query(MedialibFile).filter_by(mb_release=id).all()

    return render_template('release.html', release=release, files=files)


@release_pages.route('/<string:id>/cover-art')
@release_pages.route('/<string:id>/cover-art/<int:size>')
def cover_art(id, size=500):
    cache_dir = os.path.join(current_app.config['CACHE_DIR'], 'cover_art')
    fallback_url = url_for('static', filename='no-cover.png')

    def fetch_cover_art():
        #has_cover_art = db.session.query(CoverArt).join(Release).filter_by(gid=id).count()
        #if not has_cover_art:
        #    return None

        url = 'http://coverartarchive.org/release/%s/front' % id
        try:
            return urllib2.urlopen(url)
        except urllib2.URLError:
            return None

    return cache_image(cache_dir, fetch_cover_art, fallback_url, id, size)


@release_pages.route('/release/<string:id>.xspf')
def xspf(id):
    release = db.session.query(Release).filter_by(gid=id).\
        options(joinedload('artist_credit')).\
        options(joinedload('artist_credit.name')).\
        options(subqueryload_all('mediums.tracks.artist_credit.name')).\
        options(subqueryload_all('mediums.tracks.recording')).\
        first()
    if release is None:
        return abort(404)

    files = db.session.query(MedialibFile).filter_by(mb_release=id).\
        order_by(MedialibFile.mb_medium_no, MedialibFile.mb_track_no)

    files_index = {}
    for file in files:
        files_index.setdefault(file.mb_recording, []).append(file)

    playlist_el = etree.Element('playlist', {'version': '1'}, nsmap={None: 'http://xspf.org/ns/0/'})
    etree.SubElement(playlist_el, 'title').text = release.name.name
    etree.SubElement(playlist_el, 'creator').text = release.artist_credit.name.name
    etree.SubElement(playlist_el, 'info').text = url_for('release.index', id=id, _external=True)

    tracklist_el = etree.SubElement(playlist_el, 'trackList')
    for medium in release.mediums:
        for track in medium.tracks:
            track_el = etree.SubElement(tracklist_el, 'track')
            etree.SubElement(track_el, 'title').text = track.name.name
            etree.SubElement(track_el, 'creator').text = track.artist_credit.name.name
            etree.SubElement(track_el, 'album').text = release.name.name
            files = files_index.get(track.recording.gid)
            if files is None:
                continue
            for file in files:
                if file.mb_medium_no == medium.position and file.mb_track_no == track.position:
                    break
            etree.SubElement(track_el, 'location').text = url_for('media.file', id=file.id, _external=True)

    response = make_response(etree.tostring(playlist_el, pretty_print=True))
    response.headers['Content-Type'] = 'application/xspf+xml; charset=UTF-8'
    response.headers['Content-Disposition'] = 'attachment; filename="muziq-%s.xspf"' % id
    return response

