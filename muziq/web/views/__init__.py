from pysolr import Solr
from sqlalchemy import sql
from sqlalchemy.orm import joinedload, subqueryload
from flask import g, render_template, request, url_for, redirect
import muziq.tables as t
from muziq import db
from muziq.models import MedialibRelease, ArtistCredit


def index():
    releases = db.session.\
        query(MedialibRelease).\
        options(joinedload('mb_release')).\
        options(subqueryload('mb_release.artist_credit')).\
        options(subqueryload('mb_release.artist_credit.artists')).\
        options(joinedload('mb_release.artist_credit.artists.name')).\
        options(joinedload('mb_release.artist_credit.artists.artist'))

    num_releases = request.args.get('limit', type=int, default=10)
    last_added_releases = releases.order_by(MedialibRelease.added.desc()).limit(num_releases)
    random_release_gids = db.session.query(MedialibRelease.mb_release_id).order_by(sql.func.random()).limit(num_releases).all()
    random_releases = releases.filter(MedialibRelease.mb_release_id.in_(random_release_gids))

    return render_template('index.html',
        last_added_releases=last_added_releases,
        random_releases=random_releases)


def archive(year):
    return render_template('release.html')


def search():
    form = g.search_form
    if form.validate():
        query = form.q.data
        solr = Solr('http://192.168.50.10:8983/solr/musicbrainz')
        response = solr.query(query, defType='dismax', qf='id^5.0 kind^2.0 name^1.5 sortname alias^0.6 artist^0.7 catno label^0.6 isrc ipi iswc code barcode country^0.5', bq='kind:artist^3.0 kind:releasegroup^1.5 kind:work^1.5', rows=20)
        for result in response.results:
            result['url'] = 'http://musicbrainz.org/%s/%s' % (result['kind'].replace('releasegroup', 'release-group'), result['id'])
        return render_template('search.html', query=query, results=response.results)
    return redirect(url_for('index'))

