import os
import shutil
import subprocess
from flask import current_app, abort, redirect, send_file

def cache_image(root_cache_dir, fetch_func, fallback_url, key, size):
    if size not in current_app.config['ALLOWED_IMAGE_SIZES']:
        return abort(400)

    cache_dir = os.path.join(root_cache_dir, key[0], key[1])
    cache_file = os.path.join(cache_dir, '%s_%s.jpg' % (key, size))

    if not os.path.isfile(cache_file):
        if not os.path.isdir(cache_dir):
            os.makedirs(cache_dir)

        full_cache_file = os.path.join(cache_dir, '%s.jpg' % key)
        if not os.path.isfile(full_cache_file):
            stream = fetch_func()
            if not stream:
                return redirect(fallback_url)

            with open(full_cache_file, 'w') as file:
                shutil.copyfileobj(stream, file)

        subprocess.call(["convert", "-resize", "%dx%d" % (size, size), "-filter", "Lanczos", full_cache_file, cache_file])

    return send_file(cache_file)

