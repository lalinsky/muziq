import redis
from flask import Flask, g, url_for, request
from sqlalchemy import create_engine
from werkzeug import import_string

from muziq.web import views
from muziq.web.forms import SearchForm
from muziq.web.views.artist import artist_pages
from muziq.web.views.release import release_pages
from muziq.web.views.files import files_pages
from muziq.web.views.media import media_pages
from muziq.scrobbler.proto1 import proto1
from muziq import db


app = Flask(__name__)
app.add_url_rule('/', view_func=views.index)
app.add_url_rule('/search', view_func=views.search)
app.register_blueprint(artist_pages, url_prefix='/artist')
app.register_blueprint(release_pages, url_prefix='/release')
app.register_blueprint(files_pages, url_prefix='/files')
app.register_blueprint(media_pages, url_prefix='/media')
app.register_blueprint(proto1, url_prefix='/scrobble1')

app.config.from_object('muziq.settings')


from werkzeug.contrib.fixers import ProxyFix
app.wsgi_app = ProxyFix(app.wsgi_app)


class GlobalState(object):
    pass

s = GlobalState()


@app.before_first_request
def setup_application():
    s.redis_pool = redis.ConnectionPool(host=app.config['REDIS_HOST'],
                                        port=app.config['REDIS_PORT'])


@app.before_request
def setup_request():
    g.redis = redis.Redis(connection_pool=s.redis_pool)
    g.search_form = SearchForm(request.values)


@app.teardown_request
def close_connections(response):
    db.session.remove()


@app.context_processor
def add_custom_template_functions():
    return {
        'url_for_static': lambda x: url_for('static', filename=x),
        'search_form': g.search_form,
    }


def lengthformat(ms):
    if ms is None:
        return '?:??'
    s = round(ms / 1000.0)
    return '%d:%02d' % (s / 60, s % 60)


app.jinja_env.filters['lengthformat'] = lengthformat
