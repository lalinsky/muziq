import time
import json
import hashlib
import re
from flask import Blueprint, g, request, make_response, abort


SUPPORTED_PROTOCOLS = set(['1.2'])

CLIENTS = {
    "tst": "Test",
    "qlb": "Quod Libet",
}

USERS = {
    "lukz": "pwd",
}


proto1 = Blueprint('proto1', __name__)


def response(lines):
    resp = make_response('\n'.join(lines) + '\n')
    resp.mimetype = 'text/plain'
    return resp


def handshake_response(failed=None, interval=None):
    lines = []
    if failed is not None:
        lines.append('FAILED %s'% failed)
    if interval is not None:
        lines.append('INTERVAL %s' % interval)
    return response(lines)


def verify_client(client, client_ver):
    return client in CLIENTS


def verify_timestamp(ts):
    one_hour = 60 * 60 * 2
    current_ts = int(time.time())
    return abs(ts - current_ts) < one_hour


def md5(s):
    return hashlib.md5(s).hexdigest()


def verify_auth(user, ts, token):
    password = USERS.get(user)
    if password is None:
        return False
    expected_token = md5(md5(password) + str(ts))
    return token == expected_token


def start_session(user, client, client_ver):
    ts = int(time.time())
    key = 'user:%s:session' % user
    session_id = g.redis.get(key)
    if session_id is None or not verify_session(session_id):
        session_id = md5('%s|%s|%s|%d' % (user, client, client_ver, ts)).upper()
        prefix = 'session:%s:' % session_id
        tx = g.redis.pipeline()
        tx.set(key, session_id)
        tx.set(prefix + 'user', user)
        tx.set(prefix + 'client', client)
        tx.set(prefix + 'client_ver', client_ver)
        tx.sadd('sessions', session_id)
        tx.execute()
    return session_id


def verify_session(session_id):
    prefix = 'session:%s:' % session_id
    user, client, client_ver = g.redis.mget(
        prefix + 'user',
        prefix + 'client',
        prefix + 'client_ver')
    if not user or not client or not client_ver:
        return None
    return {'user': user, 'client': client, 'client_ver': client_ver}


@proto1.route('/')
def handshake():
    """Handle handshake requests."""
    if request.args.get('hs') != 'true':
        return response(['Muziq scobble submission system.', 'http://muziq.eu/'])
    protocol = request.args.get('p')
    if not protocol or protocol not in SUPPORTED_PROTOCOLS:
        return response([
            'FAILED Incorrect protocol version (unsupported protocol version)',
            'INTERVAL 1',
        ])
    client = request.args.get('c')
    if not client:
        return response(['FAILED Invalid/Missing Parameter(s)'])
    client_ver = request.args.get('v')
    if not client_ver:
        return response(['FAILED Invalid/Missing Parameter(s)'])
    if not verify_client(client, client_ver):
        return response(['FAILED Invalid/Missing Parameter(s)'])
    user = request.args.get('u')
    if not user:
        return response(['FAILED Invalid/Missing Parameter(s)'])
    timestamp = request.args.get('t', type=int)
    if not timestamp:
        return response(['FAILED Invalid/Missing Parameter(s)'])
    if not verify_timestamp(timestamp):
        return response(['BADTIME'])
    auth_token = request.args.get('a')
    if not auth_token:
        return response(['FAILED Invalid/Missing Parameter(s)'])
    if not verify_auth(user, timestamp, auth_token):
        return response(['BADAUTH'])
    session = start_session(user, client, client_ver)
    return response([
        'OK', session,
        'http://muziq.eu:80/scrobble1/np_1.2',
        'http://muziq.eu:80/scrobble1/protocol_1.2',
    ])


@proto1.route('/np_1.2', methods=['POST'])
def np_1_2():
    """Handle now-playing requests in protocol version 1.2."""
    session_id = request.form.get('s')
    if not session_id:
        return response(['BADSESSION'])
    session = verify_session(session_id)
    if session is None:
        return response(['BADSESSION'])
    meta = parse_scrobble(required.form, '')
    data = json.dumps(meta)
    expiry = 60 * 5
    tx = g.redis.pipeline()
    tx.setex('user:%s:np' % session['user'], data, expiry)
    tx.publish('np', session['user'])
    tx.execute()
    print "now playing", session, meta
    return response(['OK'])


def find_indexes(form):
    indexes = set()
    for name in form.keys():
        match = re.search(r'\[(\d+)\]$', name)
        if match is not None:
            i = int(match.group(1))
            indexes.add(i)
    return sorted(indexes)


def parse_scrobble(form, suffix='', full=False):
    meta = {}
    meta['artist'] = form.get('a' + suffix)
    if not meta['artist']:
        abort(response(['FAILED Invalid/Missing Parameter(s)']))
    meta['track'] = form.get('t' + suffix)
    if not meta['track']:
        abort(response(['FAILED Invalid/Missing Parameter(s)']))
    meta['album'] = form.get('b' + suffix)
    meta['track_no'] = form.get('n' + suffix)
    meta['duration'] = form.get('l' + suffix, type=int)
    meta['mb_recording_id'] = form.get('m' + suffix)
    if full:
        meta['timestamp'] = form.get('i' + suffix, type=int)
        if not meta['timestamp']:
            abort(response(['FAILED Invalid/Missing Parameter(s)']))
    return meta


@proto1.route('/protocol_1.2', methods=['POST'])
def submit_1_2():
    """Handle scrobbling requests in protocol version 1.2."""
    session_id = request.form.get('s')
    if not session_id:
        return response(['BADSESSION'])
    session = verify_session(session_id)
    if session is None:
        return response(['BADSESSION'])
    scrobbles = []
    for i in find_indexes(request.form):
        meta = parse_scrobble(request.form, '[%s]' % i, True)
        scrobbles.append(meta)
    print "scrobbled", session, scrobbles
    return response(['OK'])

