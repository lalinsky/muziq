import sys
sys.path.append('/usr/share/beets')

import os
import datetime
import logging
from dateutil.tz import tzlocal
from sqlalchemy.orm.exc import NoResultFound
from beets.mediafile import MediaFile, FileTypeError, UnreadableFileError
from muziq import db
from muziq.models import MedialibDirectory, MedialibFile

logger = logging.getLogger(__name__)



class MediaLibrary(object):

    def __init__(self, path):
        self.path = os.path.normpath(path)
        self._directory_cache = {}

    def _get_directory(self, parent, name):
        try:
            return db.session.query(MedialibDirectory).\
                filter_by(parent=parent, name=name).one()
        except NoResultFound:
            directory = MedialibDirectory()
            directory.parent = parent
            directory.name = name
            db.session.add(directory)
            return directory

    def _split_path(self, path):
        parent_path, name = os.path.split(path)
        return self.get_directory(parent_path), name

    def get_directory(self, path):
        directory = self._directory_cache.get(path)
        if directory is not None:
            return directory
        if not path:
            parent, name = None, ''
        else:
            parent, name = self._split_path(path)
        directory = self._get_directory(parent, name)
        self._directory_cache[path] = directory
        return directory

    def get_file(self, path):
        parent, name = self._split_path(path)
        try:
            return db.session.query(MedialibFile).\
                filter_by(parent=parent, name=name).one()
        except NoResultFound:
            return None

    def add_file(self, path):
        file = MedialibFile()
        file.parent, file.name = self._split_path(path)
        db.session.add(file)
        return file


class Scanner(object):

    def __init__(self, medialib):
        self.medialib = medialib

    def run(self):
        for root, dirs, files in os.walk(self.medialib.path):
            print "Scanning %s (%d files)" % (root, len(files))
            for name in files:
                path = os.path.join(root, name)
                if not path.startswith(self.medialib.path):
                    raise Exception('file path does not start with the library path')
                relative_path = path[len(self.medialib.path)+1:]
                file = self.medialib.get_file(relative_path)

                stat = os.lstat(path)
                updated = datetime.datetime.fromtimestamp(stat.st_mtime, tz=tzlocal())
                if file is not None and file.updated >= updated:
                    continue

                try:
                    mf = MediaFile(path)
                except (FileTypeError, UnreadableFileError):
                    continue

                if not mf.mb_trackid or not mf.mb_albumid:
                    continue

                if file is None:
                    file = self.medialib.add_file(relative_path)
                    file.added = updated

                file.updated = updated
                file.mb_recording = mf.mb_trackid
                file.mb_release = mf.mb_albumid
                file.mb_track_no = mf.track
                file.mb_medium_no = mf.disc

                db.session.commit()


if __name__ == '__main__':
    library = MediaLibrary(sys.argv[1])
    scanner = Scanner(library)
    scanner.run()
    db.session.remove()

