from sqlalchemy import Column, Integer, String, ForeignKey, MetaData, Table
from sqlalchemy.dialects.postgresql import UUID

meta = MetaData()

artist_name = Table('artist_name', meta,
    Column('id', Integer, primary_key=True),
    Column('name', String),
    schema='musicbrainz')

artist = Table('artist', meta,
    Column('id', Integer, primary_key=True),
    Column('gid', UUID),
    Column('name', Integer, ForeignKey('musicbrainz.artist_name.id'), key='name_id'),
    schema='musicbrainz')

artist_credit = Table('artist_credit', meta,
    Column('id', Integer, primary_key=True),
    Column('name', Integer, ForeignKey('musicbrainz.artist_name.id'), key='name_id'),
    schema='musicbrainz')

artist_credit_name = Table('artist_credit_name', meta,
    Column('artist_credit', Integer, ForeignKey('musicbrainz.artist_credit.id'), key='artist_credit_id'),
    Column('artist', Integer, ForeignKey('musicbrainz.artist.id'), key='artist_id'),
    Column('name', Integer, ForeignKey('musicbrainz.artist_name.id'), key='name_id'),
    Column('position', Integer),
    schema='musicbrainz')

label_name = Table('label_name', meta,
    Column('id', Integer, primary_key=True),
    Column('name', String),
    schema='musicbrainz')

label = Table('label', meta,
    Column('id', Integer, primary_key=True),
    Column('gid', UUID),
    Column('name', Integer, ForeignKey('musicbrainz.label_name.id'), key='name_id'),
    schema='musicbrainz')

release_name = Table('release_name', meta,
    Column('id', Integer, primary_key=True),
    Column('name', String),
    schema='musicbrainz')

release = Table('release', meta,
    Column('id', Integer, primary_key=True),
    Column('gid', UUID),
    Column('name', Integer, ForeignKey('musicbrainz.release_name.id'), key='name_id'),
    Column('artist_credit', Integer, ForeignKey('musicbrainz.artist_credit.id'), key='artist_credit_id'),
    schema='musicbrainz')

release_group_primary_type = Table('release_group_primary_type', meta,
    Column('id', Integer, primary_key=True),
    Column('name', String),
    schema='musicbrainz')

release_group = Table('release_group', meta,
    Column('id', Integer, primary_key=True),
    Column('gid', UUID),
    Column('name', Integer, ForeignKey('musicbrainz.release_name.id'), key='name_id'),
    Column('type', Integer, ForeignKey('musicbrainz.release_group_primary_type.id'), key='type_id'),
    Column('artist_credit', Integer, ForeignKey('musicbrainz.artist_credit.id'), key='artist_credit_id'),
    schema='musicbrainz')

release_group_meta = Table('release_group_meta', meta,
    Column('id', Integer, ForeignKey('musicbrainz.release_group.id'), primary_key=True),
    Column('release_count', Integer),
    Column('first_release_date_year', Integer),
    Column('first_release_date_month', Integer),
    Column('first_release_date_day', Integer),
    schema='musicbrainz')

