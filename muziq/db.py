from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from muziq import settings


engine = create_engine(settings.PGSQL_URL, echo=True)

session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
session = Session

